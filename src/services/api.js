import axios from 'axios';
import md5 from 'js-md5';
const PUBLIC_KEY = 'ffeb6ff02c837adbd6a22bcf6f34a8d3';

// deveria ser passada por um serviço em back-end
const PRIVATE_KEY = '302b2f41b4ca49d2cabb7dedbe081b74748ac23a';

const apiBasica = axios.create({
  baseURL: 'https://gateway.marvel.com/v1/public/',
});

export const carregarPersonagens = async (name) => {
  const timestamp = Number(new Date());
  const hash = md5.create();
  hash.update(timestamp + PRIVATE_KEY + PUBLIC_KEY);
  return apiBasica.get(
    `characters?ts=${timestamp}&apikey=${PUBLIC_KEY}&hash=${hash.hex()}&name=${name}`,
  );
};
export const carregarQuadrinho = async () => {
  const timestamp = Number(new Date());
  const hash = md5.create();
  hash.update(timestamp + PRIVATE_KEY + PUBLIC_KEY);
  return apiBasica.get(
    `comics?ts=${timestamp}&apikey=${PUBLIC_KEY}&hash=${hash.hex()}`,
  );
};
export const carregarQuadrinhoPorHeroi = async (id) => {
  const timestamp = Number(new Date());
  const hash = md5.create();
  hash.update(timestamp + PRIVATE_KEY + PUBLIC_KEY);
  return apiBasica.get(
    `comics?ts=${timestamp}&apikey=${PUBLIC_KEY}&hash=${hash.hex()}&characters=${id}`,
  );
};

// intercepta respostas e trata caso necessário, no caso atual não iremos tratar erros, por ser uma demonstração simples.
apiBasica.interceptors.response.use(
  function (response) {
    console.log('sucesso,', response);
    return response;
  },
  function (error) {
    //qualquer erro pode ser tratado aquii.
    console.log('falha,', error);
    return Promise.reject(error);
  },
);
