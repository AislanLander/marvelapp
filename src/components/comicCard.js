import React from 'react';
import {View} from 'react-native';
import {TouchableOpacity} from 'react-native';
import {Image} from 'react-native';
import {Text} from 'react-native';
import {ScrollView} from 'react-native';

export const ComicCard = (props) => {
  console.log(props);
  let item = props.item;
  return (
    <View style={props.card}>
      <TouchableOpacity
        onPress={() => props.navigation.push('Comic', {item})}
        style={{alignItems: 'center'}}>
        <Image
          style={{
            height: '70%',
            width: '100%',
            borderTopRightRadius: 16,
            borderTopLeftRadius: 16,
          }}
          resizeMode={'cover'}
          source={{
            uri: `${item.thumbnail.path}.${item.thumbnail.extension}`,
          }}
        />
        <Text
          style={{
            marginLeft: 10,
            fontSize: 16,
            fontWeight: 'bold',
            paddingTop: 15,
            width: '80%',
          }}>
          {item.title} - issue {item.issueNumber} - usd: {props.price}
        </Text>
      </TouchableOpacity>
      <ScrollView>
        <Text
          style={{
            marginLeft: 10,
            fontSize: 16,
            fontWeight: 'bold',
          }}>
          {props.autores}
        </Text>
      </ScrollView>
    </View>
  );
};
