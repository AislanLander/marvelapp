import React from 'react';
import {ListItem, Avatar} from 'react-native-elements';
import {FlatList} from 'react-native';

export const CharacterList = (props) => {
  const renderCharacterList = ({item}) => (
    <ListItem bottomDivider onPress={() => props.onPress(item)}>
      <Avatar
        source={{uri: `${item.thumbnail.path}.${item.thumbnail.extension}`}}
      />
      <ListItem.Content>
        <ListItem.Title>{item.name}</ListItem.Title>
        <ListItem.Subtitle>{item.id}</ListItem.Subtitle>
      </ListItem.Content>
      <ListItem.Chevron />
    </ListItem>
  );
  return (
    <FlatList
      data={props.data}
      renderItem={renderCharacterList}
      keyExtractor={({index}) => index + ''}
    />
  );
};
