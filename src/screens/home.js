import * as React from 'react';
import {StyleSheet, Dimensions} from 'react-native';
import {
  carregarPersonagens,
  carregarQuadrinho,
  carregarQuadrinhoPorHeroi,
} from '../services/api';
import Carousel from 'react-native-snap-carousel';
import {ScrollView} from 'react-native';
import {SearchBar} from 'react-native-elements';
import {CharacterList} from '../components/characterList';
import {ActivityIndicator} from 'react-native';
import {ComicCard} from '../components/comicCard';

const {width: viewportWidth, height: viewportHeight} = Dimensions.get('window');
function wp(percentage) {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
}
const slideWidth = wp(75);
const itemHorizontalMargin = wp(4);
export const sliderWidth = viewportWidth;
export const itemWidth = slideWidth + itemHorizontalMargin * 2;

export const HomeScreen = (props) => {
  const [comics, setComics] = React.useState([]);
  const [search, setSearch] = React.useState([]);
  const [characters, setCharacters] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  let timeout = React.useRef(0);
  const carousel = React.useRef(null);

  React.useEffect(() => {
    carregarQuadrinho().then((res) => setComics(res.data.data.results));
  }, []);

  const updateSearch = async (text) => {
    setSearch(text);
    // evitar chamar a api várias vezes sem necessidade
    if (timeout.current) clearTimeout(timeout.current);
    timeout.current = setTimeout(() => {
      setLoading(true);
      carregarPersonagens(text).then((res) => {
        res.data.data && setCharacters(res.data.data.results);
        setLoading(false);
      });
    }, 800);
  };
  const _onItemPress = (characterSelected) => {
    setLoading(true);
    carregarQuadrinhoPorHeroi(characterSelected.id).then((res) => {
      res.data.data && setComics(res.data.data.results);
      setLoading(false);
    });
  };

  const _renderItem = ({item, index}) => {
    let autores = '';
    let price = 'n/a';
    if (item.creators && item.creators.items) {
      autores = 'Autores: ';
      item.creators.items.map((autor) => (autores += ` ${autor.name},`));
      autores += '.';
    }
    if (item.prices && item.prices[0].price) {
      price = item.prices[0].price;
    }
    return (
      <ComicCard
        autores={autores}
        price={price}
        item={item}
        card={styles.card}
        navigation={props.navigation}
      />
    );
  };
  return (
    <ScrollView style={styles.container}>
      <SearchBar
        placeholder="Pesquisar herói..."
        onChangeText={updateSearch}
        value={search}
        lightTheme={true}
        platform={'android'}
      />
      {characters.length ? (
        <CharacterList data={characters} onPress={_onItemPress} />
      ) : null}
      {loading && <ActivityIndicator size="large" color="#00ff00" />}
      <Carousel
        ref={carousel}
        data={comics}
        renderItem={_renderItem}
        sliderWidth={Dimensions.get('window').width}
        itemWidth={itemWidth}
        inactiveSlideScale={1}
        inactiveSlideOpacity={1}
        enableSnap
        hasParallaxImages
        decelerationRate={'fast'}
        activeSlideAlignment="center"
        containerCustomStyle={styles.containerCustomStyle}
        contentContainerCustomStyle={{overflow: 'visible'}}
      />
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  container: {flex: 1},
  card: {
    backgroundColor: '#fff',
    elevation: 12,
    shadowColor: 'black',
    shadowOffset: {width: 1, height: 10},
    shadowOpacity: 0.3,
    shadowRadius: 16,
    width: Dimensions.get('window').width * 0.8,
    height: 507,
    alignSelf: 'center',
    borderRadius: 16,
    overflow: 'visible',
    marginBottom: 20,
  },
  containerCustomStyle: {marginTop: 15, overflow: 'visible'},
});
