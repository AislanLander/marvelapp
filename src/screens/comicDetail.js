import React from 'react';
import {View} from 'react-native';
import {Image} from 'react-native';
import {Text} from 'react-native';

export const ComicDetailScreen = (props) => {
  const comic = props.route.params.item;
  console.log('comic Details', comic);
  return (
    <View>
      <Image
        style={{
          height: '65%',
          width: '80%',
          alignSelf: 'center',
        }}
        resizeMode={'contain'}
        source={{
          uri: `${comic.thumbnail.path}.${comic.thumbnail.extension}`,
        }}
      />
      <Text
        style={{
          marginLeft: 10,
          fontSize: 16,
          fontWeight: 'bold',
          paddingTop: 15,
          width: '80%',
        }}>
        {comic.title} - issue {comic.issueNumber}
      </Text>
      <Text
        style={{
          marginLeft: 10,
          fontSize: 16,
          fontWeight: 'bold',
          paddingTop: 15,
          width: '80%',
        }}>
        {comic.description}
      </Text>
    </View>
  );
};
